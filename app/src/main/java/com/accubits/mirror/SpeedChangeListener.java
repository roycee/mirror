package com.accubits.mirror;

public interface SpeedChangeListener {

    public void onSpeedChanged(float newSpeedValue);

}