package com.accubits.mirror;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    RelativeLayout speedometer;
    ImageView mirror;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        speedometer = (RelativeLayout) findViewById(R.id.speedometer);
        mirror = (ImageView) findViewById(R.id.iv);

        //    speedometer.setDrawingCacheEnabled(true);
        //    speedometer.buildDrawingCache();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = getBitmapFromView(speedometer);
                mirror.setImageBitmap(flipBitmap(bitmap));
            }
        }, 4000);

    }

    private Bitmap getBitmapFromView(View view) {
        Log.i("RESP", view.getWidth() + ":" + view.getHeight());
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.GRAY);
        view.draw(canvas);
        return returnedBitmap;
    }


    private Bitmap flipBitmap(Bitmap src) {
        Matrix matrix = new Matrix();
        matrix.preScale(-1, 1);
        Bitmap dst = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, false);
        dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
        return dst;
    }
}
